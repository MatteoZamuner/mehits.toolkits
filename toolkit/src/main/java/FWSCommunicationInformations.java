import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

class FWSCommunicationInformations {

  private ArrayList<Path> fwsCommunicationInformationArrayPath;
  private ArrayList<FWSCommunicationInformation> fwsCommunicationInformationArrayList = new ArrayList<>();


  protected FWSCommunicationInformations(String directoryPath) throws IOException {
    //read information from path and populate list
    fwsCommunicationInformationArrayPath = readPathsFromDirectoryPath(directoryPath);
    for(Path path:fwsCommunicationInformationArrayPath) {
//      System.out.println(path);
      fwsCommunicationInformationArrayList.add(new FWSCommunicationInformation(path));
    }
  }

  private ArrayList<Path> readPathsFromDirectoryPath(String directoryPath) throws IOException {
    final int FILE_NUMBER = 44;
    ArrayList<Path> fwsCommunicationInformationArrayPath = new ArrayList<>();
    try (Stream<Path> paths = Files.walk(Paths.get(directoryPath))) {
      paths
              .filter(Files::isRegularFile)
              .forEach(fwsCommunicationInformationArrayPath::add);
    }

    if (fwsCommunicationInformationArrayPath.size() != FILE_NUMBER)
      throw new IOException("There aren't all W3000_* excelFile; them are only " + fwsCommunicationInformationArrayPath.size());

    return fwsCommunicationInformationArrayPath;
  }
}
