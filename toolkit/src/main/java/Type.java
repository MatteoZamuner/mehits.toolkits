import java.io.IOException;

public class Type{

  private String type;
  final private String word = "WORD";
  final private String bit = "BIT";

  public Type(String type) throws IOException {
    if(type.equals(word))
      setType(word);
    else if(type.equals(bit))
      setType(bit);
    else
      throw new IOException("Invalid Type read: " + type + " is not a valid type");
  }

  public String getType() {
    return type;
  }

  public void setType(String setType) {
    type = setType;
  }

  public boolean isWordType(){
    return this.getType().equals(word);
  }

  public boolean isBitType(){
    return this.getType().equals(bit);
  }

  @Override
  public String toString(){
    return getType();
  }

}
