import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

class FWSCommunicationInformation {

  String description = "";
  String copyDescription;
  Type type;
  String srcAddress;
  String srcIndex;
  int srcPage;
  String dstAddress;
  String dstIndex;
  int dstPage;
  int pollingPrio;
  int offlineRecheck;
  ArrayList<Integer> unitIndexes = new ArrayList<>();

  //            %INDEX%
//    ind. unità 1 : 131
//    ind. unità 2 : 143
//    ind. unità 3 : 155
//    ind. unità 4 : 167
//    ind. unità 5 : 179
//    ind. unità 6 : 191
//    ind. unità 7 : 203
//    ind. unità 8 : 215
  public FWSCommunicationInformation(Path path) throws IOException {
    try{
      BufferedReader bufferedReader = new BufferedReader(new FileReader(path.toFile()));
      String readLine;
      while ((readLine = bufferedReader.readLine()) != null) {
        interpretLine(readLine);
      }
    }catch (IOException e){
      e.printStackTrace();
    }

//    printProperties();

  }

  private void interpretLine(String line){
    if(line.startsWith("#"))
      description += line;
    else if(line.startsWith("copyDesc"))
      copyDescription = line.replaceAll("copyDesc \"(.*)\"", "$1");
    else if(checkIfLineStartWith(line, "type")){
      try{
        type = new Type(line.replaceAll("\\s+","").replaceAll("type=(.*)", "$1"));
      }catch (IOException e){
        e.printStackTrace();
      }
    }
    else if(checkIfLineStartWith(line, "srcAddress"))
      srcAddress = convertLineToStringProperty(line);
    else if(checkIfLineStartWith(line, "srcIndex"))
      srcIndex = convertLineToStringProperty(line);
    else if(checkIfLineStartWith(line, "srcPage"))
      srcPage = convertLineToIntegerProperty(line);
    else if(checkIfLineStartWith(line, "dstAddress"))
      dstAddress = convertLineToStringProperty(line);
    else if(checkIfLineStartWith(line, "dstIndex"))
      dstIndex = convertLineToStringProperty(line);
    else if(checkIfLineStartWith(line, "dstPage"))
      dstPage = convertLineToIntegerProperty(line);
    else if(checkIfLineStartWith(line, "pollingPrio"))
      pollingPrio = convertLineToIntegerProperty(line);
    else if(checkIfLineStartWith(line, "offlineRecheck"))
      offlineRecheck = convertLineToIntegerProperty(line);
    else if(checkIfLineStartWith(line, "ind.unit")){
      unitIndexes.add(convertLineToIndexProperty(line));}
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
    else if(checkIfLineStartWith(line, "ind.unit"))
      unitIndexes.add(convertLineToIndexProperty(line));
  }

  private boolean checkIfLineStartWith(String line, String start){
    //regex replaceAll("\\s+","") remove all space and tabs
    return line.replaceAll("\\s+","").startsWith(start);
  }

  private String convertLineToStringProperty(String line){
    //regex replaceAll("\\s+","") remove all space and tabs
    //regex replaceAll("[^=]*=(.*)", "$1") replace all line with only things after the equals
    return line.replaceAll("\\s+","").replaceAll("[^=]*=(.*)", "$1");
  }

  private int convertLineToIntegerProperty(String line){
    //regex replaceAll("\\s+","") remove all space and tabs
    //regex replaceAll("[^=]*=(.*)", "$1") replace all line with only things after the equals
    return Integer.parseInt(line.replaceAll("\\s+","").replaceAll("[^=]*=(.*)", "$1"));
  }

  private int convertLineToIndexProperty(String line){
    //regex replaceAll("\\s+","") remove all space and tabs
    //regex replaceAll("[^:]*:(.*)", "$1") replace all line with only things after the equals
    return Integer.parseInt(line.replaceAll("\\s+","").replaceAll("[^:]*:(.*)", "$1"));
  }

  public void printProperties(){
    System.out.println("description = " + description);
    System.out.println("copyDescription = " + copyDescription);
    System.out.println("type = " + type);
    System.out.println("srcAddress = " + srcAddress);
    System.out.println("srcIndex = " + srcIndex);
    System.out.println("srcPage = " + srcPage);
    System.out.println("dstAddress = " + dstAddress);
    System.out.println("dstIndex = " + dstIndex);
    System.out.println("dstPage = " + dstPage);
    System.out.println("pollingPrio = " + pollingPrio);
    System.out.println("offlineRecheck = " + offlineRecheck);
    for (int i=0; i<unitIndexes.size()-1; i++) {
      System.out.println("unitIndexes" + i + " = " + unitIndexes.get(i));
    }

  }
}
