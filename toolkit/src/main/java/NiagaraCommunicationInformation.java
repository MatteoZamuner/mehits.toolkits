public class NiagaraCommunicationInformation {

  String controllerVariableName;
  String readDataFrom;
  String writeDataTo;
  int controllerAddress;
  int unitAddress;

  final String UNIT = "unit";
  final String CONTROLLER = "controller";
  final String ALL_UNITS = "all_units";

  public String getControllerVariableName() {
    return controllerVariableName;
  }

  public void setControllerVariableName(String controllerVariableName) {
    this.controllerVariableName = controllerVariableName;
  }

  public String getReadDataFrom() {
    return readDataFrom;
  }

  public void setReadDataFrom(String readDataFrom) {
    this.readDataFrom = readDataFrom;
  }

  public String getWriteDataTo() {
    return writeDataTo;
  }

  public void setWriteDataTo(String writeDataTo) {
    this.writeDataTo = writeDataTo;
  }

  public int getControllerAddress() {
    return controllerAddress;
  }

  public void setControllerAddress(int controllerAddress) {
    this.controllerAddress = controllerAddress;
  }

  public int getUnitAddress() {
    return unitAddress;
  }

  public void setUnitAddress(int unitAddress) {
    this.unitAddress = unitAddress;
  }
}
