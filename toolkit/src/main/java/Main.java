import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
  public static void main(String [] args) throws IOException, InvalidFormatException {
    System.out.println("Main Start");

    LOGGER.setLevel(Level.FINE);

    final String EXCEL_FILE_NAME = "NewManager_pCO81VariablesManagement_r17My.xlsx";

    final int TITLE_ROW_INDEX = 0;
    final String CONTROLLER_VARIABLE_NAMES_TITLE = "Nome parametro";
    final String CONTROLLER_ADDRESS_TITLE = "Nuovo Indirizzo Modbus";
    final String READ_DATA_FROM_TITLE = "FWS legge il dato da";
    final String WRITE_DATA_TO_TITLE = "FWS scrive dato in";
    final String UNIT_ADDRESS_TITLE = "Indirizzo registro unità";


    FWSCommunicationInformations fwsCommunicationInformations = new FWSCommunicationInformations("D:/Work/Mehits/Manager3000/FWS_DataTransfertWithW3000");


    ExcelReader newManagerExcelFile = new ExcelReader(EXCEL_FILE_NAME);
    Workbook newManagerWorkbook = newManagerExcelFile.getWorkbook();
    Sheet variablesTableSheet = newManagerWorkbook.getSheet("VariablesTable");
    System.out.println(UNIT_ADDRESS_TITLE + " column = " + ExcelReader.findCellAtSpecificRowContainingTextInSheet(TITLE_ROW_INDEX, UNIT_ADDRESS_TITLE, variablesTableSheet));

    System.out.println("Main End");
  }

  private final static Logger LOGGER = Logger.getLogger(Main.class.getName());
}
