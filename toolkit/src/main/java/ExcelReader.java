import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;

public class ExcelReader {

  private Workbook workbook;

  public Workbook getWorkbook() {
    return workbook;
  }

  public ExcelReader(String excelFileName) throws IOException, InvalidFormatException {
    ClassLoader classLoader = this.getClass().getClassLoader();
    File excelFile = new File(Objects.requireNonNull(classLoader.getResource(excelFileName)).getFile());
//    FileInputStream excelFileStream = new FileInputStream(new File("C:/Users/zamun/IdeaProjects/mehits.toolkits/src/main/resources/" + excelFileName));
//    System.out.println(excelFileStream);
    workbook = WorkbookFactory.create(excelFile);
asd
//    Workbook workbook = new XSSFWorkbook(excelFileStream);
  }

  public void printNumberOfSheetsInWorkbook(){
    // Retrieving the number of sheets in the Workbook
    System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
  }

  public static int findCellAtSpecificRowContainingTextInSheet(int row, String text, Sheet sheet){

    String cellContent;
    int column = 0;
    do{
      cellContent = sheet.getRow(row).getCell(column).getStringCellValue();
      column += 1;
    }
    while (!cellContent.equals("") && !cellContent.equals(text));
    return column;
  }
}